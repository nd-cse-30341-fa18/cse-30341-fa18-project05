/* func_collatz.c: compute collatz */

#include "memhashed/cache.h"
#include "memhashed/queue.h"
#include "memhashed/thread.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define SENTINEL (-1)

/* Globals */

Cache  *Collatz = NULL;
Queue  *Numbers = NULL;

/* Threads */

/**
 * Continuously computes the collatz length for the data in the Numbers Queue
 * until the SENTINEL is encountered.
 */
void *	collatz_thread(void *arg) {
    // TODO: Implement worker thread
    return NULL;
}

/* Handler */

/**
 * Recursively computes collatz length for specified key.
 * @param   key	    Number to computer length for.
 * @return  Length of collatz sequence for specified key.
 */
int64_t	collatz_handler(const uint64_t key) {
    // TODO: Implement handler
    return 0;
}

/* Main execution */

int main(int argc, char *argv[]) {
    if (argc != 5) {
    	fprintf(stderr, "Usage: %s AddressLength PageSize EvictionPolicy Threads\n", argv[0]);
    	return EXIT_FAILURE;
    }

    // TODO: Parse command line arguments

    // TODO: Create Collatz Cache

    // TODO: Create Numbers Queue

    // TODO: Create Worker threads

    // TODO: Read numbers from standard input and add to Numbers queue

    // TODO: Join Worker threads

    // TODO: Output Collatz Cache statistics
    
    // TODO: Delete Collatz Cache and Numbers Queue
    return EXIT_SUCCESS;
}
