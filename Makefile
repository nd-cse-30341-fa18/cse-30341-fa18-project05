# Configuration

CC		= gcc
LD		= gcc
AR		= ar
CFLAGS		= -g -std=gnu99 -Wall -Iinclude -fPIC
LDFLAGS		= -Llib -pthread
LIBS		= -lm
ARFLAGS		= rcs

# Variables

MH_LIB_HDRS	= $(wildcard include/memhashed/*.h)
MH_LIB_SRCS	= $(wildcard src/memhashed/*.c)
MH_LIB_OBJS	= $(MH_LIB_SRCS:.c=.o)
MH_LIBRARY	= lib/libmemhashed.a

MH_TEST_SRCS    = $(wildcard src/tests/*.c)
MH_TEST_OBJS    = $(MH_TEST_SRCS:.c=.o)
MH_UNIT_TESTS	= $(patsubst src/tests/%,bin/%,$(patsubst %.c,%,$(wildcard src/tests/unit_*.c)))
MH_FUNC_TESTS   = $(patsubst src/tests/%,bin/%,$(patsubst %.c,%,$(wildcard src/tests/func_*.c)))

# Rules

all:		$(MH_LIBRARY) $(MH_UNIT_TESTS) $(MH_FUNC_TESTS)

%.o:		%.c $(MH_LIB_HDRS)
	@echo "Compiling $@"
	@$(CC) $(CFLAGS) -c -o $@ $<

$(MH_LIBRARY):	$(MH_LIB_OBJS)
	@echo "Linking   $@"
	@$(AR) $(ARFLAGS) $@ $^

bin/unit_%:	src/tests/unit_%.o $(MH_LIBRARY)
	@echo "Linking   $@"
	@$(LD) $(LDFLAGS) -o $@ $^

bin/func_%:	src/tests/func_%.o $(MH_LIBRARY)
	@echo "Linking   $@"
	@$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

test:	$(MH_UNIT_TESTS) $(MH_FUNC_TESTS)
	@for test in bin/unit_*; do 		\
	    for i in $$(seq 0 $$($$test 2>&1 | tail -n 1 | awk '{print $$1}')); do \
		echo "Running   $$(basename $$test) $$i";	\
		$$test $$i;			\
	    done				\
	done

	@for test in bin/test_*.sh; do		\
	    $$test;				\
	done

clean:
	@echo "Removing  objects"
	@rm -f $(MH_LIB_OBJS)

	@echo "Removing  libraries"
	@rm -f $(MH_LIB_LIBS) $(MH_TEST_OBJS)

	@echo "Removing  test programs"
	@rm -f $(MH_UNIT_TESTS) $(MH_FUNC_TESTS)

.PRECIOUS: %.o
